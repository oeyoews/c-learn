#include <stdio.h>

#include "math/add.h"
#include "math/divide.h"

int main(int argc, char *argv[])
{
  // TODO: how to use unicode in c
  // how to integrate lua interface
  // printf("demo\n");

  // printf version with hello
  char version[] = "v0.0.2";
  printf("Hello, Cmake! %s\n", version);

  // add.h
  int sum = add(1, 99);
  printf("Sum:  %d\n", sum);

  // divide.h
  int divsum = divide(199, 33);
  printf("Divide: %d\n", divsum);

  return 0;
}
