#include <stdio.h>

struct Date {
  int year;
  int month;
  int day;
};

struct Student {
  char* name;
  int age;
  float height;
  struct Date birthday;
} stu;

struct Student stu = { "Hone", 18, 190 };

struct Student stu_copy[100] = {
  { "zero", 8, 91.9 },
  { "second", 18, 191 },
  { "third", 28, 291 },
  { "fourth", 38, 391 },
};

int main(int argc, char* argv[])
{
  stu.birthday.day = 23;
  // "vdemo"

  int hello;
  scanf("%d", &hello);
  printf("name: %s\n", stu.name);
  printf("age: %d\n", stu.age);

  printf("array struct: %s\n", stu_copy[0].name);
  printf("array struct: %d\n", stu_copy[0].age);
}
