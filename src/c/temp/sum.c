/**
 * @file
 * @brief
 */

#include <stdio.h>

/**
 * @brief
 *
 * @param x
 * @param y
 * @return
 */
int add(int x, int y)
{
  return x + y;
}

int main()
{
  int r = add(33, 99);
  return 0;
}
