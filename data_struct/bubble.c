// Author: oeyoews
// Email: jyao4783@gmail.com
// Create Time: 2022-06-15T19:42:24
// Algorithm Name: Bubble Sort (1)
// Description: This like Abandon this english word, appear firstly every time
// Reference: https://www.runoob.com/w3cnote/bubble-sort.html

/* Example code
#include <stdio.h>
void bubble_sort(int arr[], int len)
{
  int i, j, temp;
  for (i = 0; i < len - 1; i++)
    for (j = 0; j < len - 1 - i; j++)
      if (arr[j] > arr[j + 1]) {
        temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
}
int main()
{
  int arr[] = { 22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70 };
  int len = (int)sizeof(arr) / sizeof(*arr);
  bubble_sort(arr, len);
  int i;
  for (i = 0; i < len; i++)
    printf("%d ", arr[i]);
  return 0;
}
*/

#include <stdio.h>

void bubble_sort(int arr[], int len)
{
  int i;
  int j;
  int temp;
  for (i = 0; i < len - 1; i++)
    for (j = 0; j < len - 1 - i; j++)
      if (arr[j] > arr[j + 1]) {
        temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
}

int main()
{
  int arr[] = {
    22, 3, 32, 2, 99, 1000000000,
  };
  int len = (int)sizeof(arr) / sizeof(*arr);
  bubble_sort(arr, len);
  int i;
  printf("bubble_sort:\n");
  for (i = 0; i < len; i++)
    printf("%d\n", arr[i]);
  return 0;
}

// ten: https://sort.hust.cc/1.bubblesort
