#include <stdio.h>

// i++ likely
enum DAYS { MON = 1, TUR, WEN, THU, FRI, STA, SUN } day;

void for_days()
{
  // this enum must consist, to use for loop
  for (day = MON; day <= SUN; day++) {
    printf("%u\n", day);
  }
}

int main(int argc, char *argv[])
{
  for_days();
  printf("===\n");
  enum DAYS thu = THU;
  printf("%u\n", thu);
  // variable
  day = FRI;
  printf("%u\n", day);
  printf("%u\n", MON);
  printf("%u\n", TUR);
  return 0;
}
