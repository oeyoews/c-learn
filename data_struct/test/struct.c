#include <stdio.h>
#include <stdlib.h>

/*
 * @see: https://www.geeksforgeeks.org/arrow-operator-in-c-c-with-examples/
 * @ref: https://www.zhihu.com/question/35114381
 */

// init variable
struct TEST {
  int width;
  double height;
} test = { 99, 99.00 }, t2;

typedef struct {
  char name[80];
  int age;
  float percentage;
} POINTER;

POINTER *emp = NULL;

void pointer_test()
{
  emp = (POINTER *)malloc(sizeof(POINTER));
  emp->age = 99;
  printf("pointer_test ==> %d\n", emp->age);
}

// recommend this usage
// but can't initial variable directly
typedef struct {
  char name;
  int height;
} STRTEST;

int main(int argc, char *argv[])
{
  pointer_test();
  t2.width = 99;
  printf("%d\n", t2.width);
  printf("%d\n", test.width);
  STRTEST strtest = { 'l', 199.0 };
  STRTEST second = { 'h', 9999 };
  printf("%c\n%d\n", second.name, second.height);
  printf("%d\n", strtest.height);
  printf("%c\n", strtest.name);
  return 0;
}
