<h1 align="center">ctest</h1>

<div align="center">

<img src="https://img.shields.io/badge/Lang-c-blueviolet.svg?style=flat-square&logo=c&color=90E59A&logoColor=blue" alt="lang">

<img src="https://cdn.jsdelivr.net/gh/oeyoews/img/letter-cl.png" width=64/>

</div>

<hr>

## Dependencies

* cmake
* make or ninja
* clangd
* clang-format
* gdb

## Config clang-format

```bash
clang-format -style=google -dump-config > .clang-format  ## generate .clang-format
cmake -B build -G Ninja  # generate build folder
cmake --build build
cmake --install build
```

```bash
ldd build/main
# tips
ccmake build
```

## Links

* arrow-pointer:  https://www.geeksforgeeks.org/arrow-operator-in-c-c-with-examples/
* https://www.mianshigee.com/tutorial/CMake-Cookbook/content-chapter1-1.4-chinese.md
* Videos: https://www.bilibili.com/video/BV1sW411v7VZ?p=4
* https://zhuanlan.zhihu.com/p/108502992
* https://zhuanlan.zhihu.com/p/414323404
* https://clang.llvm.org/docs/ClangFormatStyleOptions.html
* https://zhuanlan.zhihu.com/p/371257515

## TODO

* [ ] add preclean steps
* [x] config hover lsp
* [x] add c ignore template
* [ ] config multiple file
  * https://www.igiftidea.com/article/11568545317.html

## NOTES

* if you see `CMake Error: Error: generator : Ninja`, please the build or related folder, to solve this cmakecache version confilict problem

## Vscode debug

* https://blog.csdn.net/wohu1104/article/details/111464778

* `F5` start debug, or continue
* `Shift F5` close debug
* `F9`  toggle breakpoints
* `F11` step into
* `Shift F11` step out
* `F10` step over

## Cmake

* cmake ignore captical and small letter ??

## Format

```bash
clang-format -style=file -i xx/xxx.c
```
